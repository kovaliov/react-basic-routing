# README #

### Heroku 

```
https://murmuring-shore-62825.herokuapp.com/#/
```

The purpose of this project is demonstrate knowledge in ReactJS Routing.  

### Success criteria ###

* Installation
* Router
* Route
* Webpack
* Nested Routes
* IndexRedirect
* IndexRoute
* Route by default
* Linking
* URL params
* onlyActiveIndexRoute
* activeStyles
* activeClassName
* Hight component 

```