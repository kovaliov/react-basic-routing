import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, IndexRedirect, hashHistory } from 'react-router';

import App from './App';

import Home from './components/Home';
import Book from './components/Book';
import Books from './components/Books';
import About from './components/About';
import NotFound from './components/NotFound';
import Shelf from './components/Shelf';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route /* path="/" */ component={App}>

      <Route path="/" component={Home}></Route>
      <Route path="/about" component={About}></Route>

      <Route path="/books" component={Books}>
        <IndexRedirect to="/books/javascript" />

        <Route path=":topic">
          <IndexRoute component={Shelf} />

          <Route path=":slug" component={Book}></Route>
        </Route>
      </Route>

      <Route path="/*" component={NotFound}></Route>
    </Route>
  </Router>, document.getElementById('root'));
